<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login','UserController@login');

Route::group(['prefix' => 'jobs','middleware' => 'auth:api'], function () {
    Route::get('index', 'JobsController@index');
    Route::get('show/{id}', 'JobsController@show');
    Route::post('store', 'JobsController@store');
    Route::get('destroy/{id}', 'JobsController@destroy');
});

Route::group(['prefix' => 'managements','middleware' => 'auth:api'], function () {
    Route::get('index', 'ManagementsController@index');
    Route::get('show/{nim}', 'ManagementsController@show');
    Route::post('store', 'ManagementsController@store');
});

Route::group(['prefix' => 'events','middleware' => 'auth:api'], function () {
    Route::get('index', 'EventsController@index');
    Route::get('show/{id}', 'EventsController@show');
    Route::post('store', 'EventsController@store');
});

Route::group(['prefix' => 'presences','middleware' => 'auth:api'], function () {
    Route::get('index', 'PresencesController@index');
    Route::get('show/{id}', 'PresencesController@show');
    Route::post('store', 'PresencesController@store');
    Route::get('destroy/{id}', 'PresencesController@destroy');
});