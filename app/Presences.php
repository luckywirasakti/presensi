<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Presences extends Model
{
    protected $guarded = ['id','created_at','updated_at'];
    
    public function managements()
    {
        return $this->belongsTo('App\Managements', 'nim');
    }
}
