<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Managements extends Model
{
    protected $primaryKey = "nim";
    
    public $incrementing = false;

    protected $guarded = ['created_at', 'updated_at'];

    public function jobs()
    {
        return $this->belongsTo('App\Jobs', 'jobs_id', 'id');
    }

    protected $hidden = ['jobs_id', 'created_at', 'updated_at'];

}
