<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Events extends Model
{
    
    protected $guarded = ["id", "created_at", "updated_at"];

    public function attendances()
    {
    	return $this->belongsToMany(Managements::class, 'presences', 'events_id', 'managements_nim');
    }

    protected $hidden = ['updated_at'];
}
