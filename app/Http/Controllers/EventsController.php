<?php

namespace App\Http\Controllers;

use App\Events;
use Illuminate\Http\Request;

class EventsController extends Controller
{
    public function index()
    {
        return Events::orderBy('created_at','DESC')->get();
    }

    public function store(Request $request)
    {
        $query = Events::create($request->all());

        if($query){
            return response([
                'event_id' => $query->id,
                'message' => 'Record added!',
                'success' => true
            ],200);
        }

        return response([
            'message' => 'Oops something wrong!',
            'success' => false
        ],200);
    }

    public function show(Events $events, $id)
    {
        if(Events::find($id)){
            return Events::find($id);
        } else {
            return response([
                'message' => 'Record not Found!',
                'success' => false
            ],200);
        }

        return response([
            'message' => 'Oops something wrong!',
            'success' => false
        ],200);
    }

    public function update(Request $request, Events $events)
    {

    }
}
