<?php

namespace App\Http\Controllers;

use App\Jobs;
use Illuminate\Http\Request;

class JobsController extends Controller
{
    public function index()
    {
        return Jobs::all();
    }

    public function store(Request $request)
    {
        $query = Jobs::create($request->all());

        if($query){
            return response([
                'message' => 'Record added!',
                'success' => true
            ],200);
        }
        
        return response([
            'message' => 'Oops something wrong!',
            'success' => false
        ],200);

    }

    public function show($id)
    {
        if(Jobs::find($id)){
            return Jobs::find($id);
        } else {
            return response([
                'message' => 'Record not Found!',
                'success' => false
            ],200);
        }

        return response([
            'message' => 'Oops something wrong!',
            'success' => false
        ],200);
    }

    public function update(Request $request, $id)
    {
        
    }

    public function destroy($id)
    {
        if(Jobs::find($id)){
            if(Jobs::destroy($id)){
                return response([
                    'message' => 'Record removed!',
                    'success' => true
                ],200);
            }
        } else {
            return response([
                'message' => 'Resource not found!',
                'success' => false
            ],200);    
        }
        
        return response([
            'message' => 'Oops something wrong!',
            'success' => false
        ],200);
    }
}
