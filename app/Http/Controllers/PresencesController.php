<?php

namespace App\Http\Controllers;

use App\Events;
use App\Presences;
use App\Managements;
use Illuminate\Http\Request;

class PresencesController extends Controller
{
    public function index()
    {
        return Events::with('attendances')->orderBy('created_at', 'DESC')->get();
    }

    public function store(Request $request)
    {
        if(Presences::where('managements_nim',$request->managements_nim)
            ->where('events_id', Events::latest()->first()->id)->first()){
            
            return response([
                'message' => 'Record avaliable!',
                'success' => true
            ],200);

        } else {
            if(Managements::find($request->managements_nim)){
                if(Presences::create([
                    'managements_nim' => $request->managements_nim,
                    'events_id' => Events::latest()->first()->id])){
                        
                    return response([
                        'message' => 'Record added!',
                        'success' => true
                    ],200);
                } 
            } else {
        
                return response([
                    'message' => 'Action failed!',
                    'success' => false
                ],200);
            }
            
        }

        return response([
            'message' => 'Oops something wrong!',
            'success' => false
        ],200);
    }


    public function show($id)
    {
        return Events::with('attendances')->where('id',$id)->firstOrFail();
    }

    public function destroy($id)
    {
        if(Presences::find($id)){
            if(Presences::destroy($id)){
                return response([
                    'message' => 'Record removed!',
                    'success' => true
                ],200);
            }
        } else{
            return response([
                'message' => 'Resource not found!',
                'success' => false
            ],200);    
        }
        
        return response([
            'message' => 'Oops something wrong!',
            'success' => false
        ],200);
    }
}
