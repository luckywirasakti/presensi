<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function login(){
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){
            $user = Auth::user();

            $token = Str::random(80);

            $user->api_token = hash('sha256', $token);
            $user->save();
            $year = explode(".",$user->nim);
            $nim = str_replace(".","_",$user->nim);
            $user['photo'] = "http://www.amikom.ac.id/public/fotomhs/20".$year[0]."/".$nim.".jpg";
            $user['token'] = "Bearer ".$token;

            return response([
                'success' => true,
                'data' => $user
            ],200);
        } else {
            
            return response([
                'success' => false,
                'message' => 'Credential must be correct!'
            ],400);
        }
        
        return response(['error'=>'Unauthorised'], 400);
        
    }
}
