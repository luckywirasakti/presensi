<?php

namespace App\Http\Controllers;

use App\Jobs;
use App\Managements;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ManagementsController extends Controller
{

    public function index()
    {
        return Managements::with('jobs')->get();
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nim' => 'unique:managements',
            'phone' => 'unique:managements',
            'email' => 'unique:managements',
        ]);

        if(Jobs::find($request->jobs_id)){
            
            if ($validator->fails()) {
                return response([
                    'message' => 'nim, phone, email must be unique.',
                    'success' => true
                ],200);
            }
    
    
            if(Managements::create($request->all())){
                return response([
                    'message' => 'Record added!',
                    'success' => true
                ],200);
            }
        } else {
            return response([
                'message' => 'Job not avaliable!',
                'success' => false
            ],200);
        }
        

        return response([
            'message' => 'Oops something wrong!',
            'success' => false
        ],200);
    }

    public function show($nim)
    {
        if(Managements::find($nim)){
            return Managements::find($nim);
        } else {
            return response([
                'message' => 'Record not Found!',
                'success' => false
            ],200);
        }

        return response([
            'message' => 'Oops something wrong!',
            'success' => false
        ],200);
    }

    public function update(Request $request,$nim)
    {
        $validator = Validator::make($request->all(), [
            'nim' => 'unique:managements',
            'phone' => 'unique:managements',
            'email' => 'unique:managements',
        ]);

        if(Jobs::find($request->jobs_id)){
            if ($validator->fails()) {
                return response([
                    'message' => 'nim, phone, email must be unique.',
                    'success' => true
                ],200);
            }
    
    
            if(Managements::update($request->except(['nim']))){
                return response([
                    'message' => 'Record updated!',
                    'success' => true
                ],200);
            }
        } else {
            return response([
                'message' => 'Job not avaliable!',
                'success' => false
            ],200);
        }
        

        return response([
            'message' => 'Oops something wrong!',
            'success' => false
        ],200);
    }
}
